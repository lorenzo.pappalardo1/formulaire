<?php 
session_start();
require_once 'helpers.php'; 
$user = 'root'; 
$password = null;
if(post_exist(['prenom','nom'])){
    $prenom = $_POST['prenom'];
    $nom = $_POST['nom']; 

}else{
    header('Location: /formulaire/update.php?id='.$_SESSION['id']);
    
}

if($_SESSION['prenom'] != $prenom || $_SESSION['nom'] != $nom){
    try{
        $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
        $stmt = $dbh->prepare("UPDATE utilisateurs SET nom = :nom, prenom = :prenom WHERE id = :id");  
        $stmt->bindParam(':nom',$nom);
        $stmt->bindParam(':prenom',$prenom);
        $stmt->bindParam(':id',$_SESSION['id']);
        $stmt->execute();
        header('Location: /formulaire/');
    }
    catch(Exception $e){
        var_dump($e);
    }
}else{
    header('Location: /formulaire/update.php?id='.$_SESSION['id']);
}